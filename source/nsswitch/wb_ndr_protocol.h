/*
   Unix SMB/CIFS implementation.

   Copyright (C) Stefan Metzmacher 2007

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "nsswitch/winbind_client.h"
#include "librpc/gen_ndr/ndr_winbind_protocol.h"

bool winbind_ndr_request_response(TALLOC_CTX *mem_ctx,
				  uint32_t opnum,
				  void *r,
				  int need_priv);
