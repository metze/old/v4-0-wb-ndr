/*
   Unix SMB/CIFS implementation.

   Copyright (C) Stefan Metzmacher 2007

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "nsswitch/wb_ndr_protocol.h"

#define WB_NDR_CHECK(call) do { \
	NTSTATUS _status; \
	_status = call; \
	if (!NT_STATUS_IS_OK(_status)) { \
		return false; \
	} \
} while (0)

#define WB_NDR_NOMEM(ptr) do { \
	if (!ptr) { \
		return false; \
	} \
} while (0)


static bool winbind_ndr_send_request(TALLOC_CTX *mem_ctx,
				     uint32_t opnum,
				     void *r,
				     int need_priv)
{
	struct ndr_push *p;
	const struct ndr_interface_call *c;
	struct winbind_header h;
	DATA_BLOB b;
	ssize_t ret;

	/* Check for our tricky environment variable */

	if (winbind_env_set()) {
		return false;
	}

	if (opnum >= ndr_table_winbind_protocol.num_calls) {
		return false;
	}

	c = &ndr_table_winbind_protocol.calls[opnum];

	p = ndr_push_init_ctx(mem_ctx);
	WB_NDR_NOMEM(p);

	ZERO_STRUCT(h);
	h.opnum	= opnum;

	/* push header */
	WB_NDR_CHECK(ndr_push_STRUCT_winbind_header(p,
						    NDR_SCALARS|NDR_BUFFERS,
						    &h));

	/* push payload */
	WB_NDR_CHECK(c->ndr_push(p, NDR_IN, r));

	b = ndr_push_blob(p);

	/* fix length */
	RSIVAL(b.data, 0,
	       b.length - 4);

	ret = winbind_write_sock(b.data, b.length, 0, need_priv);
	talloc_free(p);
	if (ret == -1) {
		return false;
	}

	return true;
}

static bool winbind_ndr_recv_response(TALLOC_CTX *mem_ctx,
				      uint32_t opnum,
				      void *r)
{
	struct ndr_pull *p;
	const struct ndr_interface_call *c;
	struct winbind_header h;
	uint8_t d[8];
	ssize_t ret;
	DATA_BLOB b;
	uint32_t length;
	uint32_t min_length = WINBIND_HEADER_SIZE - 4;
	uint32_t max_length = 0xFFFFFFFF - 4;

	if (opnum >= ndr_table_winbind_protocol.num_calls) {
		return false;
	}

	c = &ndr_table_winbind_protocol.calls[opnum];

	ret = winbind_read_sock(d, sizeof(d));
	if (ret == -1) {
		return false;
	}

	if (strncmp("WBPT", (const char *)&d[4], 4) != 0) {
		return false;
	}

	length = RIVAL(&d[0], 0);

	if (length < min_length) {
		return false;
	}

	if (length > max_length) {
		return false;
	}

	b = data_blob_talloc(mem_ctx, NULL, length + 4);
	WB_NDR_NOMEM(b.data);

	memcpy(b.data, d, sizeof(d));

	ret = winbind_read_sock(b.data + sizeof(d), b.length - sizeof(d));
	if (ret == -1) {
		return false;
	}

	p = ndr_pull_init_blob(&b, mem_ctx);
	WB_NDR_NOMEM(p);

	/* pull header */
	WB_NDR_CHECK(ndr_pull_STRUCT_winbind_header(p,
						    NDR_SCALARS|NDR_BUFFERS,
						    &h));

	if (!(h.flags & WINBIND_HEADER_FLAGS_RESPONSE)) {
		return false;
	}

	if (h.flags & WINBIND_HEADER_FLAGS_ERROR) {
		enum winbind_status result;
		WB_NDR_CHECK(ndr_pull_winbind_status(p,
						     NDR_SCALARS|NDR_BUFFERS,
						     &result));
		return false;
	} else {
		WB_NDR_CHECK(c->ndr_pull(p, NDR_OUT, r));
	}

	talloc_free(b.data);
	talloc_free(p);
	return true;
}

bool winbind_ndr_request_response(TALLOC_CTX *mem_ctx,
				  uint32_t opnum,
				  void *r,
				  int need_priv)
{
	bool ret = false;
	uint32_t count = 0;

	while ((ret == false) && (count < 10)) {
		ret = winbind_ndr_send_request(mem_ctx, opnum, r, need_priv);
		if (ret == false) {
			return ret;
		}
		ret = winbind_ndr_recv_response(mem_ctx, opnum, r);
		count += 1;
	}

	return ret;
}
