/*
   Unix SMB/CIFS implementation.
   SMB torture tester - winbind struct based protocol
   Copyright (C) Stefan Metzmacher 2007

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "torture/torture.h"
#include "torture/winbind/proto.h"
#include "nsswitch/wb_ndr_protocol.h"
#include "param/param.h"

#define DO_NDR_REQ_REP(op,r) do { \
	bool _ok; \
	_ok = winbind_ndr_request_response(torture, op, r, 0); \
	torture_assert(torture, _ok, __STRING(op)); \
	torture_assert_int_equal(torture, (r)->out.result, WINBIND_STATUS_OK, \
				 __STRING(op)); \
} while (0)

static bool torture_winbind_ndr_ping(struct torture_context *torture)
{
	struct timeval tv = timeval_current();
	int timelimit = torture_setting_int(torture, "timelimit", 5);
	uint32_t total = 0;

	torture_comment(torture, "Running NDR_WINBIND_PING for %d seconds\n",
			timelimit);

	while (timeval_elapsed(&tv) < timelimit) {
		struct winbind_ping r;
		DO_NDR_REQ_REP(NDR_WINBIND_PING, &r);
		total++;
	}

	torture_comment(torture, "%u (%.1f/s) NDR_WINBIND_PING\n",
			total, total / timeval_elapsed(&tv));

	return true;
}

static bool torture_winbind_ndr_get_dc_info(struct torture_context *torture)
{
	struct winbind_get_dc_info r;
	enum winbind_dc_info_level level;

	ZERO_STRUCT(r);
	r.in.level	= &level;

	*r.in.level		= WINBIND_DC_INFO_LEVEL_COMPAT_NT4;
	r.in.domain_name	= lp_workgroup();
	r.in.dc_name		= NULL;

	DO_NDR_REQ_REP(NDR_WINBIND_GET_DC_INFO, &r);

	torture_assert_int_equal(torture, *r.out.level,
				 WINBIND_DC_INFO_LEVEL_COMPAT_NT4,
				 __STRING(WINBIND_GET_DC_INFO));

	torture_assert(torture, r.out.dc_info->name,
		       __STRING(WINBIND_GET_DC_INFO));

	/*
	 * TODO: test name string not only the pointer
	 */
	return true;
}

struct torture_suite *torture_winbind_ndr_init(void)
{
	struct torture_suite *suite = torture_suite_create(talloc_autofree_context(), "NDR");

	torture_suite_add_simple_test(suite, "PING", torture_winbind_ndr_ping);
	torture_suite_add_simple_test(suite, "GETDCINFO", torture_winbind_ndr_get_dc_info);

	suite->description = talloc_strdup(suite, "WINBIND - ndr based protocol tests");

	return suite;
}
