#!gmake
#
CC = @CC@
prefix = @prefix@
exec_prefix = @exec_prefix@
datarootdir = @datarootdir@
includedir = @includedir@
libdir = @libdir@
bindir = @bindir@
sbindir = @sbindir@
mandir = @mandir@
localstatedir = @localstatedir@
VPATH = @srcdir@:@tdbdir@:@tallocdir@:@libreplacedir@:@poptdir@
srcdir = @srcdir@
etcdir = @sysconfdir@
builddir = @builddir@
DESTDIR = /
EXTRA_OBJ=@EXTRA_OBJ@
XSLTPROC = /usr/bin/xsltproc
INSTALLCMD = @INSTALL@

POPT_LIBS = @POPT_LIBS@
POPT_CFLAGS = @POPT_CFLAGS@
POPT_OBJ = @POPT_OBJ@

CFLAGS=-g -I$(srcdir)/include -Iinclude -Ilib -Ilib/util -I$(srcdir) \
       -I@tallocdir@ -I@tdbdir@/include -I@libreplacedir@ \
	-DVARDIR=\"$(localstatedir)\" -DETCDIR=\"$(etcdir)\" \
	-DUSE_MMAP=1 @CFLAGS@ $(POPT_CFLAGS)

LIB_FLAGS=@LDFLAGS@ -Llib @LIBS@ $(POPT_LIBS) @INFINIBAND_LIBS@

UTIL_OBJ = lib/util/idtree.o lib/util/db_wrap.o lib/util/strlist.o lib/util/util.o \
	lib/util/util_time.o lib/util/util_file.o

CTDB_COMMON_OBJ =  common/ctdb_io.o common/ctdb_util.o \
	common/ctdb_ltdb.o common/ctdb_message.o common/cmdline.o  \
	lib/util/debug.o common/system.o

CTDB_TCP_OBJ = tcp/tcp_connect.o tcp/tcp_io.o tcp/tcp_init.o

CTDB_CLIENT_OBJ = client/ctdb_client.o \
	$(CTDB_COMMON_OBJ) $(POPT_OBJ) $(UTIL_OBJ) @TALLOC_OBJ@ @TDB_OBJ@ \
	@LIBREPLACEOBJ@ $(EXTRA_OBJ) @EVENTS_OBJ@ 

CTDB_SERVER_OBJ = server/ctdbd.o server/ctdb_daemon.o server/ctdb_lockwait.o \
	server/ctdb_recoverd.o server/ctdb_recover.o server/ctdb_freeze.o \
	server/ctdb_tunables.o server/ctdb_monitor.o server/ctdb_server.o \
	server/ctdb_control.o server/ctdb_call.o server/ctdb_ltdb_server.o \
	server/ctdb_traverse.o server/eventscript.o server/ctdb_takeover.o \
	$(CTDB_CLIENT_OBJ) $(CTDB_TCP_OBJ) @INFINIBAND_WRAPPER_OBJ@

TEST_BINS=bin/ctdb_bench bin/ctdb_fetch bin/ctdb_store @INFINIBAND_BINS@
BINS = bin/ctdb bin/scsi_io
SBINS = bin/ctdbd

DIRS = lib bin

.SUFFIXES: .c .o .h .1 .1.xml .1.html

all: showflags dirs doc $(CTDB_SERVER_OBJ) $(CTDB_CLIENT_OBJ) $(BINS) $(SBINS) $(TEST_BINS)

showflags:
	@echo 'ctdb will be compiled with flags:'
	@echo '  CFLAGS = $(CFLAGS)'
	@echo '  LIBS = $(LIBS)'

.c.o:
	@echo Compiling $*.c
	@mkdir -p `dirname $@`
	@$(CC) $(CFLAGS) -c $< -o $@

dirs:
	@mkdir -p $(DIRS)

bin/ctdbd: $(CTDB_SERVER_OBJ)
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ $(CTDB_SERVER_OBJ) $(LIB_FLAGS)

bin/scsi_io: $(CTDB_CLIENT_OBJ) scsi/scsi_io.o 
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ scsi/scsi_io.o $(CTDB_CLIENT_OBJ) $(LIB_FLAGS)

bin/ctdb: $(CTDB_CLIENT_OBJ) tools/ctdb.o 
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ tools/ctdb.o $(CTDB_CLIENT_OBJ) $(LIB_FLAGS)

bin/ctdb_bench: $(CTDB_CLIENT_OBJ) tests/ctdb_bench.o 
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ tests/ctdb_bench.o $(CTDB_CLIENT_OBJ) $(LIB_FLAGS)

bin/ctdb_fetch: $(CTDB_CLIENT_OBJ) tests/ctdb_fetch.o 
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ tests/ctdb_fetch.o $(CTDB_CLIENT_OBJ) $(LIB_FLAGS)

bin/ctdb_store: $(CTDB_CLIENT_OBJ) tests/ctdb_store.o 
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ tests/ctdb_store.o $(CTDB_CLIENT_OBJ) $(LIB_FLAGS)

bin/ibwrapper_test: $(CTDB_CLIENT_OBJ) ib/ibwrapper_test.o
	@echo Linking $@
	@$(CC) $(CFLAGS) -o $@ ib/ibwrapper_test.o $(CTDB_CLIENT_OBJ) $(LIB_FLAGS)

.1.xml.1:
	-test -z "$(XSLTPROC)" || $(XSLTPROC) -o $@ http://docbook.sourceforge.net/release/xsl/current/manpages/docbook.xsl $<

.1.xml.1.html:
	-test -z "$(XSLTPROC)" || $(XSLTPROC) -o $@ http://docbook.sourceforge.net/release/xsl/current/html/docbook.xsl $<

doc: doc/ctdb.1 doc/ctdb.1.html \
	doc/ctdbd.1 doc/ctdbd.1.html \
	doc/onnode.1 doc/onnode.1.html

clean:
	rm -f *.o */*.o */*/*.o */*~
	rm -f $(BINS) $(SBINS) $(TEST_BINS)

distclean: clean
	rm -f *~ */*~
	rm -rf bin
	rm -f config.log config.status config.cache config.h
	rm -f Makefile

install: all
	mkdir -p $(DESTDIR)$(bindir)
	mkdir -p $(DESTDIR)$(sbindir)
	mkdir -p $(DESTDIR)$(includedir)
	mkdir -p $(DESTDIR)$(etcdir)/ctdb
	mkdir -p $(DESTDIR)$(etcdir)/ctdb/events.d
	${INSTALLCMD} -m 755 bin/ctdb $(DESTDIR)$(bindir)
	${INSTALLCMD} -m 755 bin/ctdbd $(DESTDIR)$(sbindir)
	${INSTALLCMD} -m 644 include/ctdb.h $(DESTDIR)$(includedir)
	${INSTALLCMD} -m 644 include/ctdb_private.h $(DESTDIR)$(includedir) # for samba3
	${INSTALLCMD} -m 755 config/events $(DESTDIR)$(etcdir)/ctdb
	${INSTALLCMD} -m 755 config/functions $(DESTDIR)$(etcdir)/ctdb
	${INSTALLCMD} -m 755 config/statd-callout $(DESTDIR)$(etcdir)/ctdb
	${INSTALLCMD} -m 755 config/events.d/10.interface $(DESTDIR)$(etcdir)/ctdb/events.d
	${INSTALLCMD} -m 755 config/events.d/40.vsftpd $(DESTDIR)$(etcdir)/ctdb/events.d
	${INSTALLCMD} -m 755 config/events.d/50.samba $(DESTDIR)$(etcdir)/ctdb/events.d
	${INSTALLCMD} -m 755 config/events.d/60.nfs $(DESTDIR)$(etcdir)/ctdb/events.d
	${INSTALLCMD} -m 755 config/events.d/61.nfstickle $(DESTDIR)$(etcdir)/ctdb/events.d
	${INSTALLCMD} -m 755 tools/onnode.ssh $(DESTDIR)$(bindir)
	${INSTALLCMD} -m 755 tools/onnode.rsh $(DESTDIR)$(bindir)
	if [ -f doc/ctdb.1 ];then ${INSTALLCMD} -d $(DESTDIR)$(mandir)/man1; fi
	if [ -f doc/ctdb.1 ];then ${INSTALLCMD} -m 644 doc/ctdb.1 $(DESTDIR)$(mandir)/man1; fi
	if [ -f doc/ctdbd.1 ];then ${INSTALLCMD} -m 644 doc/ctdbd.1 $(DESTDIR)$(mandir)/man1; fi
	if [ -f doc/onnode.1 ];then ${INSTALLCMD} -m 644 doc/onnode.1 $(DESTDIR)$(mandir)/man1; fi
	cd $(DESTDIR)$(bindir) && ln -sf onnode.ssh onnode

test: all
	tests/run_tests.sh

valgrindtest: all
	VALGRIND="valgrind -q --trace-children=yes" tests/run_tests.sh


realdistclean: distclean
	rm -f configure config.h.in
